set "port=3000"
set /p port="port (default 3000): "

set "memory=64m"
set /p memory="memory (default 64m): "

docker pull eartymiuk444/earty-info-static-image
docker rm -f earty-info-static
docker run -dp %port%:80 --name earty-info-static  -m %memory% eartymiuk444/earty-info-static-image
pause