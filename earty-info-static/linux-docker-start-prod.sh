#! /bin/bash
set -x

read -e -p "port [3000]: " port
port=${port:-3000}
echo $port

read -e -p "memory limit [64m]: " memoryLimit
memoryLimit=${memoryLimit:-64m}
echo $memoryLimit

sudo docker pull eartymiuk444/earty-info-static-image
sudo docker rm -f earty-info-static
sudo docker run -dp "$port":80 --name earty-info-static -m "$memoryLimit" eartymiuk444/earty-info-static-image