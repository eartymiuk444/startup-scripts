set "dockerPropertiesFile=C:\apps\projects\eai\eai-config\environment-properties\mongo\local-mongo.properties"
set /p dockerPropertiesFile="docker properties file (default 'C:\apps\projects\eai\eai-config\environment-properties\mongo\local-mongo.properties'): "
set "port=27017"
set /p port="port (default 27017): "

set "memory=384m"
set /p memory="memory (default 384m): "

docker pull mongo:5.0.5-focal
docker rm -f mongo
docker run -dp %port%:27017 --name mongo -m %memory% --env-file %dockerPropertiesFile% mongo:5.0.5-focal
pause