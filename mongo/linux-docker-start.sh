#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/mongo/prod-mongo.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/mongo/prod-mongo.properties}
echo $dockerPropertiesFile

read -e -p "port [27017]: " port
port=${port:-27017}
echo $port

read -e -p "memory limit [384m]: " memoryLimit
memoryLimit=${memoryLimit:-384m}
echo $memoryLimit

sudo docker pull mongo:5.0.5-focal
sudo docker rm -f mongo
sudo docker run -dp "$port":27017 --name mongo -m "$memoryLimit" --env-file "$dockerPropertiesFile" mongo:5.0.5-focal