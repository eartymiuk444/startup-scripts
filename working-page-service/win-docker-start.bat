set "dockerPropertiesFile=C:\Apps\projects\eai\eai-config\environment-properties\working-page-service\local-docker-working-page-service.properties"
set /p dockerPropertiesFile="docker properties file (default 'C:\Apps\projects\eai\eai-config\environment-properties\working-page-service\local-docker-working-page-service.properties'): "

set "port=8084"
set /p port="port (default 8084): "

set "memory=192m"
set /p memory="memory (default 192m): "

set "heap-start=128m"
set /p heap-start="heap-start (default 128m): "
set "heap-limit=192m"
set /p heap-limit="heap-limit (default 192m): "
set "stack-limit=256k"
set /p stack-limit="stack-limit (default 256k): "

docker pull eartymiuk444/working-page-service-image
docker rm -f working-page-service
docker run -dp %port%:8080 --name working-page-service -m %memory% -e JAVA_TOOL_OPTIONS="-Xms%heap-start% -Xmx%heap-limit% -Xss%stack-limit%" --env-file %dockerPropertiesFile% eartymiuk444/working-page-service-image
pause