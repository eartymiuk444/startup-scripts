#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/working-page-service/prod-working-page-service.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/working-page-service/prod-working-page-service.properties}
echo $dockerPropertiesFile

read -e -p "port [8084]: " port
port=${port:-8084}
echo $port

read -e -p "memory limit [192m]: " memoryLimit
memoryLimit=${memoryLimit:-192m}
echo $memoryLimit

read -e -p "java heap start [128m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-128m}
echo $javaHeapStart

read -e -p "java heap limit [192m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-192m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/working-page-service-image
sudo docker rm -f working-page-service
sudo docker run -dp "$port":8080 --name working-page-service  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/working-page-service-image