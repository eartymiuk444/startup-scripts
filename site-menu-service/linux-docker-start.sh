#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/site-menu-service/prod-site-menu-service.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/site-menu-service/prod-site-menu-service.properties}
echo $dockerPropertiesFile

read -e -p "port [8081]: " port
port=${port:-8081}
echo $port

read -e -p "memory limit [192m]: " memoryLimit
memoryLimit=${memoryLimit:-192m}
echo $memoryLimit

read -e -p "java heap start [128m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-128m}
echo $javaHeapStart

read -e -p "java heap limit [192m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-192m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/site-menu-service-image
sudo docker rm -f site-menu-service
sudo docker run -dp "$port":8080 --name site-menu-service  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/site-menu-service-image