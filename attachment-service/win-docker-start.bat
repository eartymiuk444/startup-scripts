set "dockerPropertiesFile=C:\Apps\projects\eai\eai-config\environment-properties\attachment-service\local-docker-attachment-service.properties"
set /p dockerPropertiesFile="docker properties file (default 'C:\Apps\projects\eai\eai-config\environment-properties\attachment-service\local-docker-attachment-service.properties'): "

set "port=8083"
set /p port="port (default 8083): "

set "memory=256m"
set /p memory="memory (default 256m): "

set "heap-start=192m"
set /p heap-start="heap-start (default 192m): "
set "heap-limit=256m"
set /p heap-limit="heap-limit (default 256m): "
set "stack-limit=256k"
set /p stack-limit="stack-limit (default 256k): "

docker pull eartymiuk444/attachment-service-image
docker rm -f attachment-service
docker run -dp %port%:8080 --name attachment-service -m %memory% -e JAVA_TOOL_OPTIONS="-Xms%heap-start% -Xmx%heap-limit% -Xss%stack-limit%" --env-file %dockerPropertiesFile% eartymiuk444/attachment-service-image
pause