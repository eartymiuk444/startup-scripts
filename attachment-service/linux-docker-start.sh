#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/attachment-service/prod-attachment-service.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/attachment-service/prod-attachment-service.properties}
echo $dockerPropertiesFile

read -e -p "port [8083]: " port
port=${port:-8083}
echo $port

read -e -p "memory limit [256m]: " memoryLimit
memoryLimit=${memoryLimit:-256m}
echo $memoryLimit

read -e -p "java heap start [192m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-192m}
echo $javaHeapStart

read -e -p "java heap limit [256m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-256m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/attachment-service-image
sudo docker rm -f attachment-service
sudo docker run -dp "$port":8080 --name attachment-service  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/attachment-service-image