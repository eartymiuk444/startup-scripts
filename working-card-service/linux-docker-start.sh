#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/working-card-service/prod-working-card-service.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/working-card-service/prod-working-card-service.properties}
echo $dockerPropertiesFile

read -e -p "port [8085]: " port
port=${port:-8085}
echo $port

read -e -p "memory limit [320m]: " memoryLimit
memoryLimit=${memoryLimit:-320m}
echo $memoryLimit

read -e -p "java heap start [288m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-288m}
echo $javaHeapStart

read -e -p "java heap limit [320m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-320m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/working-card-service-image
sudo docker rm -f working-card-service
sudo docker run -dp "$port":8080 --name working-card-service  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/working-card-service-image