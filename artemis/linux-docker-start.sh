#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/artemis/prod-artemis.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/artemis/prod-artemis.properties}
echo $dockerPropertiesFile

read -e -p "port [61616]: " port
port=${port:-61616}
echo $port

read -e -p "management port [8161]: " managementPort
managementPort=${managementPort:-8161}
echo $managementPort

read -e -p "broker folder [~/eai/artemis-instance]: " brokerFolder
brokerFolder=${brokerFolder:-~/eai/artemis-instance}
echo $brokerFolder

read -e -p "artemis configuration [~/eai/eai-config/artemis-configuration/broker.xml]: " artemisConfig
artemisConfig=${artemisConfig:-~/eai/eai-config/artemis-configuration/broker.xml}
echo $artemisConfig

read -e -p "memory limit [512m]: " memoryLimit
memoryLimit=${memoryLimit:-512m}
echo $memoryLimit

sudo docker pull eartymiuk444/artemis-ibm-semeru-runtimes-open-11-image
sudo docker rm -f artemis
sudo docker run -dp "$port":61616 -p "$managementPort":8161 -v "$brokerFolder":/var/lib/artemis-instance --name artemis -m "$memoryLimit" --env-file "$dockerPropertiesFile" eartymiuk444/artemis-ibm-semeru-runtimes-open-11-image
sudo docker stop artemis
sudo cp "$artemisConfig" "$brokerFolder"/etc/broker.xml
sudo docker start artemis