set "dockerPropertiesFile=C:\Apps\projects\eai\eai-config\environment-properties\artemis\local-artemis.properties"
set /p dockerPropertiesFile="docker properties file (default 'C:\Apps\projects\eai\eai-config\environment-properties\artemis\local-artemis.properties'): "

set "port=61616"
set /p port="port (default 61616): "

set "managementPort=8161"
set /p managementPort="port (default 8161): "

set "brokerFolder=C:\Apps\artemis-instance"
set /p brokerFolder="broker folder (default C:\Apps\artemis-instance): "

set "artemisConfig=C:\Apps\projects\eai\eai-config\artemis-configuration\broker.xml"
set /p artemisConfig="artemis configuration (default C:\Apps\projects\eai\eai-config\artemis-configuration\broker.xml):"

set "memory=512m"
set /p memory="memory (default 512m): "

docker pull eartymiuk444/artemis-ibm-semeru-runtimes-open-11-image
docker rm -f artemis
docker run -dp %port%:61616 -p %managementPort%:8161 -v %brokerFolder%:/var/lib/artemis-instance --name artemis -m %memory% --env-file %dockerPropertiesFile% eartymiuk444/artemis-ibm-semeru-runtimes-open-11-image
docker stop artemis
copy /Y %artemisConfig% %brokerFolder%\etc\broker.xml
docker start artemis
pause