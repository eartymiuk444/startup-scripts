#! /bin/bash
set -x

read -e -p "broker folder [~/eai/artemis-instance]: " brokerFolder
brokerFolder=${brokerFolder:-~/eai/artemis-instance}
echo $brokerFolder

sudo docker stop artemis
sudo rm -rf "$brokerFolder"
sudo mkdir -m u=rwx,g=rwx,o=rwx "$brokerFolder"