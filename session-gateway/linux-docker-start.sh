#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/session-gateway/prod-session-gateway.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/session-gateway/prod-session-gateway.properties}
echo $dockerPropertiesFile

read -e -p "port [8080]: " port
port=${port:-8080}
echo $port

read -e -p "memory limit [192m]: " memoryLimit
memoryLimit=${memoryLimit:-192m}
echo $memoryLimit

read -e -p "java heap start [128m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-128m}
echo $javaHeapStart

read -e -p "java heap limit [192m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-192m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/session-gateway-image
sudo docker rm -f session-gateway
sudo docker run -dp "$port":8080 --name session-gateway  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/session-gateway-image