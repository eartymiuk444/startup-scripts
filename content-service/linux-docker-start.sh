#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/content-service/prod-content-service.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/content-service/prod-content-service.properties}
echo $dockerPropertiesFile

read -e -p "port [8086]: " port
port=${port:-8086}
echo $port

read -e -p "memory limit [384m]: " memoryLimit
memoryLimit=${memoryLimit:-384m}
echo $memoryLimit

read -e -p "java heap start [256m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-256m}
echo $javaHeapStart

read -e -p "java heap limit [256m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-256m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/content-service-image
sudo docker rm -f content-service
sudo docker run -dp "$port":8080 --name content-service  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/content-service-image