set "dockerPropertiesFile=C:\Apps\projects\eai\eai-config\environment-properties\content-service\local-docker-content-service.properties"
set /p dockerPropertiesFile="docker properties file (default 'C:\Apps\projects\eai\eai-config\environment-properties\content-service\local-docker-content-service.properties'): "

set "port=8086"
set /p port="port (default 8086): "

set "memory=512m"
set /p memory="memory (default 512m): "

set "heap-start=384m"
set /p heap-start="heap-start (default 384m): "
set "heap-limit=384m"
set /p heap-limit="heap-limit (default 384m): "
set "stack-limit=384k"
set /p stack-limit="stack-limit (default 384k): "

docker pull eartymiuk444/content-service-image
docker rm -f content-service
docker run -dp %port%:8080 --name content-service -m %memory% -e JAVA_TOOL_OPTIONS="-Xms%heap-start% -Xmx%heap-limit% -Xss%stack-limit%" --env-file %dockerPropertiesFile% eartymiuk444/content-service-image
pause