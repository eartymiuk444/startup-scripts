#! /bin/bash
set -x

read -e -p "docker properties file [~/eai/eai-config/environment-properties/image-service/prod-image-service.properties]: " dockerPropertiesFile
dockerPropertiesFile=${dockerPropertiesFile:-~/eai/eai-config/environment-properties/image-service/prod-image-service.properties}
echo $dockerPropertiesFile

read -e -p "port [8082]: " port
port=${port:-8082}
echo $port

read -e -p "memory limit [256m]: " memoryLimit
memoryLimit=${memoryLimit:-256m}
echo $memoryLimit

read -e -p "java heap start [192m]: " javaHeapStart
javaHeapStart=${javaHeapStart:-192m}
echo $javaHeapStart

read -e -p "java heap limit [256m]: " javaHeapLimit
javaHeapLimit=${javaHeapLimit:-256m}
echo $javaHeapLimit

read -e -p "java stack limit [256k]: " javaStackLimit
javaStackLimit=${javaStackLimit:-256k}
echo $javaStackLimit

sudo docker pull eartymiuk444/image-service-image
sudo docker rm -f image-service
sudo docker run -dp "$port":8080 --name image-service  -m "$memoryLimit" -e JAVA_TOOL_OPTIONS="-Xms$javaHeapStart -Xmx$javaHeapLimit -Xss$javaStackLimit" --env-file "$dockerPropertiesFile" eartymiuk444/image-service-image